from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    CategoriesListView,
    AccountsListView,
    CategoriesCreateView,
    AccountsCreateView,
)


urlpatterns = [
    path("", ReceiptListView, name="home"),
    path("create/", ReceiptCreateView, name="create_receipt"),
    path("categories/", CategoriesListView, name="list_categories"),
    path("accounts/", AccountsListView, name="list_accounts"),
    path("accounts/create/", AccountsCreateView, name="create_account"),
    path("categories/create/", CategoriesCreateView, name="create_category"),
]
