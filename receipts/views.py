from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, AccountForm, ExpenseCategoryForm

# Create your views here.


@login_required
def ReceiptListView(request):
    context = {
        "receipt_list": Receipt.objects.filter(purchaser=request.user),
    }
    return render(request, "receipts/list.html", context)


@login_required
def ReceiptCreateView(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "receipts/create.html", context)


@login_required
def AccountsListView(request):
    context = {
        "accounts_list": Account.objects.filter(owner=request.user),
    }
    return render(request, "accounts/list.html", context)


@login_required
def AccountsCreateView(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.owner = request.user
            receipt.save()
            return redirect("list_accounts")

    else:
        form = AccountForm()

    context = {"form": form}

    return render(request, "accounts/create.html", context)


@login_required
def CategoriesListView(request):
    context = {
        "categories_list": ExpenseCategory.objects.filter(owner=request.user),
    }
    return render(request, "categories/list.html", context)


@login_required
def CategoriesCreateView(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.owner = request.user
            receipt.save()
            return redirect("list_categories")

    else:
        form = ExpenseCategoryForm()

    context = {"form": form}

    return render(request, "categories/create.html", context)
